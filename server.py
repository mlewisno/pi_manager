import json
import logging

import flask
from werkzeug.exceptions import HTTPException, default_exceptions


def make_json_app(import_name, **kwargs):
    """
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "405: Method Not Allowed" }

    [copy paste from http://flask.pocoo.org/snippets/83/]
    """
    def make_json_error(ex):
        response = flask.jsonify(message=str(ex))
        response.status_code = (ex.code
                                if isinstance(ex, HTTPException)
                                else 500)
        return response

    app = flask.Flask(import_name, **kwargs)

    for code in default_exceptions.iterkeys():
        app.error_handler_spec[None][code] = make_json_error

    return app


app = make_json_app(__name__)
#app.config.from_pyfile("gateway_flask_config.py")

@app.route("/healthcheck/", methods=('GET',))
def healthcheck():
    '''ELB healthcheck endpoint'''
    return _ret_msg('OK', 200)

def _ret_msg(message, code):
    if not isinstance(message, dict):
        message = {"message": message}
        response = flask.make_response(json.dumps(message), code)
        response.content_type = "application/json"
        return response


if __name__ == '__main__':
    app.run(host="0.0.0.0")
